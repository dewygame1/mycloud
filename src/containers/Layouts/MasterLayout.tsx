import * as React from 'react'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import CssBaseline from '@mui/material/CssBaseline'
import Divider from '@mui/material/Divider'
import Drawer from '@mui/material/Drawer'
import IconButton from '@mui/material/IconButton'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import MenuIcon from '@mui/icons-material/Menu'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import theme from '../../theme'
import logo from '../../assets/logo.png'
import { Outlet } from 'react-router-dom'

export interface Props {
  window?: () => Window
}

const drawerWidth = 240
const navItems = [
  'หน้าแรก',
  'เกี่ยวกับเรา',
  'บริการของเรา',
  'โปรโมชั่น',
  'บทความทั้งหมด',
]

const MasterLayout: React.FC = (props: Props) => {
  const { window } = props
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState)
  }

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography
        variant="h6"
        sx={{ my: 2 }}
        fontWeight={theme.typography.fontWeightLight}
      >
        MY CLOUD
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item} disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }}>
              <ListItemText
                sx={{
                  color: '#252525',
                  fontWeight: theme.typography.fontWeightLight,
                  letterSpacing: '1px',
                  cursor: 'pointer',
                  backgroundRepeat: 'no-repeat',
                  borderRadius: '0px',
                  animation: 'ani2 0.7s steps(29) forwards',
                  '&:hover': {
                    animation: 'ani 0.7s steps(29) forwards',
                    color: '#fff',
                    // backgroundImage: 'linear-gradient(#fff, #CA8765, #fff)',
                    background: '#CA8765',
                    fontWeight: theme.typography.fontWeightMedium,
                  },
                }}
                primary={item}
              />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  )

  const container =
    window !== undefined ? () => window().document.body : undefined

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar component="nav">
        <Toolbar sx={{ display: 'flex', alignItems: 'center' }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h5"
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
            fontWeight={theme.typography.fontWeightLight}
          >
            <img src={logo} alt="Logo" width={250} height={75} />
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'flex', height: '100px' } }}>
            {navItems.map((item) => (
              <Button
                key={item}
                sx={{
                  marginLeft: '10px',
                  marginRight: '10px',
                  color: '#252525',
                  fontWeight: theme.typography.fontWeightLight,
                  letterSpacing: '1px',
                  cursor: 'pointer',
                  backgroundRepeat: 'no-repeat',
                  borderRadius: '0px',
                  animation: 'ani2 0.7s steps(29) forwards',
                  '&:hover': {
                    animation: 'ani 0.7s steps(29) forwards',
                    color: '#fff',
                    // backgroundImage: 'linear-gradient(#fff, #CA8765, #fff)',
                    background: '#CA8765',
                    fontWeight: theme.typography.fontWeightMedium,
                  },
                }}
              >
                {item}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
      {/* <Box component="main"
      >
        <Toolbar />
      </Box> */}
      <main>
        <Toolbar />
        <Outlet />
      </main>
    </Box>
  )
}

export default MasterLayout
