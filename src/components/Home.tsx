import Typography from '@mui/material/Typography'

import {
  Button,
  Card,
  CardContent,
  CardMedia,
  Grid,
  List,
  ListItem,
  ListItemText,
  styled,
} from '@mui/material'
import Carousel from 'react-material-ui-carousel'
import theme from '../theme'
import FacebookIcon from '@mui/icons-material/Facebook'
import InstagramIcon from '@mui/icons-material/Instagram'
import { FacebookEmbed } from 'react-social-media-embed'

const Home = () => {
  const Keyframes = styled('div')({
    '@keyframes gradient': {
      '0%': {
        backgroundPosition: '0% 50%',
      },
      '50%': {
        backgroundPosition: '100% 50%',
      },
      '100%': {
        backgroundPosition: '0% 50%',
      },
    },
  })
  const imageUrl =
    'https://images.pexels.com/photos/6621308/pexels-photo-6621308.jpeg'
  const imageUrlList = [
    'https://images.pexels.com/photos/6620882/pexels-photo-6620882.jpeg?auto=compress&cs=tinysrgb&w=800',
    'https://images.pexels.com/photos/6621462/pexels-photo-6621462.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    'https://images.pexels.com/photos/6621282/pexels-photo-6621282.jpeg?auto=compress&cs=tinysrgb&w=800',
    'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
  ]
  const slides = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  ]

  const products = [
    {
      name: 'Product 1',
      description: 'Description for Product 1',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
    {
      name: 'Product 2',
      description: 'Description for Product 2',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
    {
      name: 'Product 3',
      description: 'Description for Product 3',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
    {
      name: 'Product 4',
      description: 'Description for Product 4',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
    {
      name: 'Product 5',
      description: 'Description for Product 5',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
    {
      name: 'Product 6',
      description: 'Description for Product 6',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
    {
      name: 'Product 7',
      description: 'Description for Product 7',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
    {
      name: 'Product 8',
      description: 'Description for Product 8',
      imageUrl:
        'https://images.pexels.com/photos/6998599/pexels-photo-6998599.jpeg?auto=compress&cs=tinysrgb&w=800',
    },
  ]

  const articles = [
    {
      title: 'คลินิกเสริมความงาม',
      content: `My Cloud เป็น คลินิกเสริมความงาม ที่สั่งสมประสบการณ์ด้านการปรับรูปหน้ามาอย่างยาวนาน เราเน้นย้ำเรื่องความปลอดภัยเป็นอันดับหนึ่งเสมอ และการใช้ผลิตภัณฑ์ที่ได้มาตรฐาน ตัวยาเปิดใหม่ผสมต่อหน้าผู้ใช้บริการ ตรวจสอบเลข Lot. การผลิตได้ทุกขวด ใส่ใจในทุกขั้นตอนการบริการ โดยคำนึงถึงความคุ้มค่า สวยอย่างปลอดภัย ไม่แพง ด้วยทีมแพทย์มากประสบการณ์ และมีเป้าหมายเพื่อเป็น คลินิกความงาม ที่ได้มาตรฐานระดับโลก เพื่อตอบสนองความต้องการของผู้ใช้บริการที่ให้ความไว้วางใจ คลินิกความงาม ของเราเสมอมาพร้อมทั้งพัฒนาเทคนิคการปรับรูปหน้า การเสริมความงามและนำนวัตกรรมใหม่ ๆ มาใช้ให้ทันสมัยอยู่ตลอดเวลา เพื่อให้มั่นใจได้ว่าทุกท่านที่เข้ามาใช้บริการจะได้รับความพึงพอใจที่สุดครับ`,
    },
  ]

  const youtubeVideos = [
    { title: 'Video 1', videoId: 'f1J38FlDKxo?si=0kAr91JnuEqXxvyd' },
  ]

  const articlesSixth = [
    {
      title: 'Article 1',
      imageUrl: 'https://via.placeholder.com/150',
      description: 'Description of article 1...',
    },
    {
      title: 'Article 2',
      imageUrl: 'https://via.placeholder.com/150',
      description: 'Description of article 2...',
    },
    {
      title: 'Article 2',
      imageUrl: 'https://via.placeholder.com/150',
      description: 'Description of article 2...',
    },
    {
      title: 'Article 2',
      imageUrl: 'https://via.placeholder.com/150',
      description: 'Description of article 2...',
    },
    {
      title: 'Article 2',
      imageUrl: 'https://via.placeholder.com/150',
      description: 'Description of article 2...',
    },
    {
      title: 'Article 2',
      imageUrl: 'https://via.placeholder.com/150',
      description: 'Description of article 2...',
    },
    // Add more articles as needed
  ]

  const popularQuestionsUrls = [
    'https://example.com/question1',
    'https://example.com/question2',
    'https://example.com/question3',
    'https://example.com/question4',
  ]
  const hasMoreThanFourArticles = articlesSixth.length > 4

  return (
    <div>
      {/* Header Section */}
      <div
        style={{
          position: 'relative',
          maxWidth: '100%',
          maxHeight: '80vh',
          overflow: 'hidden',
        }}
      >
        <img
          src={imageUrl}
          alt="Landing Page"
          style={{ width: '100%', height: 'auto', objectFit: 'cover' }}
        />
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.3)',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            color: '#fff',
          }}
        >
          <Typography
            variant="h2"
            gutterBottom
            style={{
              fontWeight: 700,
              fontSize: '3.5rem',
              textShadow: '2px 2px 4px rgba(0,0,0,0.5)',
              textAlign: 'center',
            }}
          >
            เราคือผู้เชี่ยวชาญด้านความงาม
          </Typography>
          <Typography
            variant="subtitle1"
            gutterBottom
            style={{
              fontSize: '1.5rem',
              textShadow: '2px 2px 4px rgba(0,0,0,0.5)',
            }}
          >
            ให้เราดูแลคุณ
          </Typography>
          <Button
            variant="contained"
            color="primary"
            style={{
              marginTop: '20px',
              boxShadow: '2px 2px 4px rgba(0,0,0,0.5)',
            }}
          >
            เสริมความงาม
          </Button>
        </div>
      </div>

      <Grid
        container
        spacing={2}
        style={{
          padding: '5% 40px 10% 40px',
          backgroundColor: '#fff',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Grid item xs={2} sm={3}></Grid>
        <Grid item xs={8} sm={6} style={{ justifyContent: 'center' }}>
          <Typography
            variant="h4"
            gutterBottom
            sx={{
              fontStyle: 'italic',
              fontSize: '1.3rem',
              lineHeight: '2rem',
              textAlign: 'center',
              paddingBottom: '5%',
            }}
          >
            <span style={{ color: '#CA8765' }}>My Cloud</span>{' '}
            เป็นคลินิกเสริมความงาม ที่สั่งสมประสบการณ์ด้านการปรับรูปหน้ามา
            อย่างยาวนาน เราเน้นเรื่องความปลอดภัย เป็นอันดับหนึ่งเสมอ
            และการใช้ผลิตภัณฑ์ที่ ได้มาตรฐาน ด้วยทีมแพทย์มากประสบการณ์
            และมีเป้าหมายเพื่อเป็นคลินิกความงามที่ได้ มาตรฐานระดับโลก
          </Typography>
          <Grid container spacing={2}>
            {imageUrlList.map((imageUrl, index) => (
              <Grid item xs={12} sm={3} key={index}>
                <Card
                  style={{
                    borderRadius: '110px 110px 0 0',
                    backgroundColor: '#fff',
                  }}
                >
                  <CardContent style={{ padding: 0 }}>
                    <img
                      src={imageUrl}
                      alt={`Image${index}`}
                      style={{
                        width: '100%',
                        borderRadius: '110px 110px 0 0',
                      }}
                    />
                    <Typography variant="subtitle1" align="center">
                      บริการที่ {index + 1}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
        <Grid item xs={2} sm={3}></Grid>
      </Grid>
      {/* Third Section */}
      <Keyframes>
        <Grid
          container
          spacing={2}
          style={{
            padding: '5% 40px 10% 40px',
            backgroundColor: '#fff',
            backgroundImage:
              'linear-gradient(-45deg, #7f6859, #e0c3a8, #fff6e5, #ffd9c6, #fff)',
            backgroundSize: '400% 400%',
            animation: 'gradient 15s ease infinite',
            //   animation: `myEffectExit 5s ease infinite`,

            display: 'flex',
            justifyContent: 'center',
            //   backgroundColor: '#ffd9c6',
          }}
        >
          <Grid item xs={0} sm={3}></Grid>
          <Grid item xs={12} sm={6} style={{ justifyContent: 'center' }}>
            <Typography
              variant="h4"
              gutterBottom
              sx={{
                fontSize: '1.6rem',
                lineHeight: '2rem',
                textAlign: 'center',
                paddingBottom: '5%',
              }}
            >
              โปรโมชั่น
            </Typography>
            <Grid container spacing={0}>
              {products.map((product, index) => (
                <Grid item xs={6} sm={3} key={index}>
                  <Card
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      justifyContent: 'space-between',
                      height: '100%',
                      borderRadius: '0px',
                      border: '1px solid #000',
                      backgroundColor: 'transparent',
                      backgroundSize: '30px',
                      transition: 'background-color 0.3s ease-in-out',
                      boxShadow: 'none',
                      '&:hover': {
                        backgroundColor: '#fff',
                      },
                    }}
                  >
                    <CardContent style={{ textAlign: 'center' }}>
                      <img
                        src={product.imageUrl}
                        alt={product.name}
                        style={{ width: '100%' }}
                      />
                      <Typography
                        variant="h6"
                        gutterBottom
                        style={{ textAlign: 'center' }}
                      >
                        {product.name}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        style={{
                          textAlign: 'center',
                          overflow: 'hidden',
                          textOverflow: 'ellipsis',
                          whiteSpace: 'nowrap',
                        }}
                      >
                        {product.description}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Grid>
          <Grid item xs={0} sm={3}></Grid>
        </Grid>
      </Keyframes>

      <Grid
        container
        spacing={2}
        style={{
          padding: '10%',
          backgroundColor: '#fff',
          alignItems: 'center',
        }}
      >
        <Grid item xs={0} sm={2}></Grid>

        {/* Articles */}
        <Grid item xs={12} sm={4}>
          {articles.map((article, index) => (
            <div key={index} style={{ marginBottom: '20px' }}>
              <Typography variant="h6" style={{ textAlign: 'center' }}>
                {article.title}
              </Typography>
              <Typography variant="body1" style={{ textAlign: 'center' }}>
                {article.content}
              </Typography>
            </div>
          ))}
        </Grid>

        {/* YouTube Videos */}
        <Grid item xs={12} sm={4}>
          {youtubeVideos.map((video, index) => (
            <div key={index}>
              <iframe
                width="100%"
                height="315"
                src={`https://www.youtube.com/embed/${video.videoId}`}
                title={video.title}
                allowFullScreen
              ></iframe>
            </div>
          ))}
        </Grid>
        <Grid item xs={0} sm={2}></Grid>
      </Grid>

      <Grid
        container
        spacing={2}
        style={{
          padding: '5% 40px 10% 40px',
          backgroundColor: '#fff',
          backgroundImage:
            'linear-gradient(-45deg, #7f6859, #e0c3a8, #fff6e5, #ffd9c6, #fff)',
          backgroundSize: '400% 400%',
          animation: 'gradient 15s ease infinite',
          //   animation: `myEffectExit 5s ease infinite`,

          display: 'flex',
          justifyContent: 'center',
          //   backgroundColor: '#ffd9c6',
        }}
      >
        <Grid item xs={0} sm={3}></Grid>
        <Grid item xs={12} sm={6} style={{ justifyContent: 'center' }}>
          <Typography
            variant="h4"
            gutterBottom
            sx={{
              fontSize: '1.6rem',
              lineHeight: '2rem',
              textAlign: 'center',
              paddingBottom: '5%',
            }}
          >
            รีวิวจากลูกค้า
          </Typography>
          <Carousel>
            {slides.map((slide) => (
              <Typography
                variant="h4"
                gutterBottom
                sx={{
                  fontStyle: 'italic',
                  fontSize: '1.3rem',
                  lineHeight: '2rem',
                  textAlign: 'center',
                  height: '10rem',
                  backgroundColor: theme.palette.primary.main,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  padding: '0 20px',
                }}
              >
                "{slide}"
              </Typography>
            ))}
          </Carousel>
        </Grid>
        <Grid item xs={0} sm={3}></Grid>
      </Grid>

      <>
        <Grid
          container
          spacing={3}
          style={{
            backgroundColor: '#fff',
            alignItems: 'center',
            padding: '5% 40px 0% 40px',
          }}
        >
          <Grid item xs={3} sm={3}></Grid>
          <Grid item xs={6} sm={6} style={{ justifyContent: 'center' }}>
            <Typography
              variant="h4"
              gutterBottom
              sx={{
                fontSize: '1.6rem',
                lineHeight: '2rem',
                textAlign: 'center',
                paddingBottom: '5%',
              }}
            >
              [ บทความ ]
            </Typography>
          </Grid>
          <Grid item xs={3} sm={3}></Grid>
        </Grid>
        <Grid
          container
          spacing={3}
          style={{
            padding: '0% 20% 10% 20%',
            backgroundColor: '#fff',
            alignItems: 'center',
          }}
        >
          {articlesSixth.slice(0, 3).map(
            (
              article,
              index // Only render first 3 articles in the first row
            ) => (
              <Grid item xs={12} sm={3} md={3} key={index}>
                <Card>
                  <CardMedia
                    component="img"
                    height="140"
                    image={article.imageUrl}
                    alt={article.title}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {article.title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      {article.description}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            )
          )}
          {/* Display "Read more" button in the last card of the first row if there are more than 4 articles */}
          {hasMoreThanFourArticles && (
            <Grid
              item
              xs={12}
              sm={3}
              md={3}
              style={{ display: 'flex', justifyContent: 'center' }}
            >
              <Typography variant="body2" color="textSecondary">
                <Button
                  variant="outlined"
                  sx={{
                    color: '#ca8765',
                    fontSize: '16px',
                    fontWeight: 500,
                    letterSpacing: '1px',
                    padding: '13px 20px',
                    outline: 0,
                    border: '1px solid black',
                    cursor: 'pointer',
                    position: 'relative',
                    backgroundColor: '#fff',
                    userSelect: 'none',
                    WebkitUserSelect: 'none',
                    touchAction: 'manipulation',
                    '&:hover': {
                      backgroundColor: '#ca8765',
                      color: '#fff',
                    },
                  }}
                >
                  บทความทั้งหมด
                </Button>
              </Typography>
            </Grid>
          )}
        </Grid>
      </>
      <Grid
        container
        spacing={0}
        style={{
          backgroundColor: '#ffd9c6',
          //   backgroundImage:
          //     'linear-gradient(-45deg, #7f6859, #e0c3a8, #fff6e5, #ffd9c6, #fff)',
          backgroundSize: '400% 400%',
          paddingTop: '3%',
          paddingBottom: '3%',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Grid item xs={12} sm={1}></Grid>
        <Grid item xs={12} sm={2}>
          <Typography variant="h6">Popular Questions</Typography>
          <List>
            {popularQuestionsUrls.map((url, index) => (
              <ListItem
                key={index}
                component="a"
                href={url}
                target="_blank"
                rel="noopener noreferrer"
                sx={{ padding: '0px' }}
              >
                <ListItemText primary={`Question ${index + 1}`} />
              </ListItem>
            ))}
          </List>
        </Grid>

        <Grid item xs={12} sm={2}>
          <Typography variant="h6">Our Services</Typography>
          <List>
            {popularQuestionsUrls.map((url, index) => (
              <ListItem
                key={index}
                component="a"
                href={url}
                target="_blank"
                rel="noopener noreferrer"
                sx={{ padding: '0px' }}
              >
                <ListItemText primary={`Services ${index + 1}`} />
              </ListItem>
            ))}
          </List>
        </Grid>

        <Grid item xs={12} sm={2}>
          <Typography variant="h6">Contact Us</Typography>
          <FacebookIcon />
          <InstagramIcon />
          <FacebookIcon />
          <InstagramIcon />
        </Grid>

        <Grid item xs={12} sm={3}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <FacebookEmbed
              url="https://www.facebook.com/andrewismusic/posts/451971596293956"
              width={300}
            />
          </div>
        </Grid>
        <Grid item xs={12} sm={1}></Grid>
      </Grid>
    </div>
  )
}

export default Home
