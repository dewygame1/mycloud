import { FC } from 'react'
import { Routes, Route, BrowserRouter } from 'react-router-dom'
import Error404 from '../containers/Error404'
import MasterLayout from '../containers/Layouts/MasterLayout'
import Home from '../components/Home'

const AppRoute: FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="*" element={<MasterLayout />}>
          <Route path="*" element={<Home />} />
          <Route path="error/*" element={<Error404 />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export { AppRoute }
